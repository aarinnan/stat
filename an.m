function [output,mod]=an( Y, X, sig, int, boot)
%[output,mod] = an( Y, X, sig, int, boot)
%
% Runs an ANOVA test on one Y-variable. The groupings are given in X.
%
% INPUT:
%  Y      The dependable variable
%  X      Groupings. Number of rows equals the length of Y. Should go from 1
%          and up.
%  sig    Significance level. Default is 1 (the whole table is shown)
%  int    The number of interactions taken into account. Default = All
%  boot   true if the estimates are from a bootstrap run. < default =
%          false>
%
% OUTPUT:
%  output The numbers given in the ANOVA table
%  mod    ANOVA table as shown
%
% See also: EQMEAN

% Need to make some further tests so that the error-term does not run to a
% negative value, or equal to 0.

% AAR 040107 Changed 'mncn' to 'cen_std'
% AAR 200905 Some serious changes to the code, making it more efficient and
%             also making it work for more than one interaction
% AAR 230503

[r,k]=size( X);

%Reorganize X, making it have running numbers for all groups
for ck = 1:k
    X( :, ck) = repnum( X( :, ck));
end

if min(size(Y))~=1
    error(['''Y'' cannot have more than one column'])
end

if nargin<4
    int=k;
end

if isempty( int)
    int = k;
end

if nargin < 5
    boot = false;
end

if isempty( boot)
    boot = false;
end

if int==k
    i=fjernlike(X);
    if size(i,1)==r
        disp(['Including the ' num2str(k) 'th interaction will set E=0'])
        disp(['Number of interactions reduced to ' (num2str(k-1))])
        int=int-1;
    end
end

if nargin<3
    sig=1;
end

if isempty( sig)
    sig = 1;
end

if sig<0 || sig>1
    error('Significance level should be between 0 and 1')
end

a=perms(1:k);

%Try something rather different concerning the interactions
testint=diag(ones(k,1));

inter=[0 ones(1,k)];
% intstr=cellstr(num2str([0;(1:k)']));
sam=cellstr([ones(k,1)*'Y' num2str((1:k)')]);
for cc = 1:size( X, 2)
    dof( cc) = length( fjernlike( X( :, cc) ) ) - 1;
end
if int>1
    for i=2:int
        b=sortrows(fjernlike(a,1:i));
        b=sortrows(fjernlike(sort(b(:,1:i)')'));
        for j=1:size(b,1)
            X(:,end+1) = repnum( X( :, b( j, :) ) ); %str2num(char(strrep(cellstr(num2str(X(:,b(j,:)))),' ','')));
            dof( end + 1) = prod( dof( b( j, :) ) );
            testint(end+1,b(j,:))=1;
            inter(:,end+1)=i;
            temp=vec([ones(i,1)*'Y' num2str(b(j,:)') ones(i,1)*'*']')';
            sam{end+1}=temp(1:end-1);
        end
    end
end
testint=[1 zeros(1,size(testint,2));ones(size(testint,1),1) testint];

% Calculate the effect of the different interactions and the degrees of
% freedom
X=[ones(r,1) X];
for i=1:size(X,2)
    if length( fjernlike( X( :, i) ) ) == 1
%         temp = [0 1];
        eff( :, i) = ones( r, 1) * mean( Y);
    else
        [ ~, eff(:,i)]=repmean(Y,X(:,i));
    end
%     %Degrees Of Freedom
%     dof(i)=length(temp)-1;
%     if inter(1,i)>1 % To correct for those dof's already used in smaller interactions
%         dof(i)=dof(i)-sum(dof(testint(i,:)==1));
%     end
end

testint=[(1:size(testint,1))' testint];
% dof(1)=[];
%Sum of Squares of Total
sst=sum(cen_std(Y).^2);
for i=2:size(inter,2)
    %In case the interaction is equal to one of the lesser interactions,
    %the ssq is set to zero
    if dof(i-1)>0
        %Find the lesser interactions
        k=find(testint(i,:)==0);
        temp=testint;
        for j=1:length(k)
            temp(temp(:,k(j))==1,:)=[];
        end
        j = temp(:,1);
        %Set the sign to the correct value
        xm = eff( :, j) * diag( (-1).^( 1 + inter( 1, i) - inter( 1, j)));
        %Sum of Squares    
        ssq(i-1)=sum( sum(xm, 2).^2);
    else
        ssq(i-1)=0;
    end
end
if boot
    doe = sqrt( r - 1 - sum( dof) );
else
    %Degrees of freedom for the error term
    doe=r-1-sum(dof);
end
%Squared sum of errors
sse=sst-sum(ssq);
if sse<0
    error('You have too many interactions! Run again with fewer')
end
%Mean squares
mq=ssq./dof;
%Mean error
me=sse/doe;
%F-values
fval=mq/me;
%Pr > F
pr=1-pf(fval,dof,doe);

%Making the output
lab={'Source','DOF','SSQ','Mean Square','F-value','Pr>F'};
data(:,1)=[dof';doe;r-1];
data(:,2)=[ssq';sse;sst];
data(:,3)=[mq';me;NaN];
data(:,4)=[fval';NaN;NaN];
data(:,5)=[pr';NaN;NaN];
sam=[sam;{'Error';'Total'}];
tab=maketable(data,lab,sam);
output=data;
mod=tab;