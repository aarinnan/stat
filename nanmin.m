function [a,i]=nanmin(x,dim, opt)
% [a,i]=nanmin(x, dim, opt)
%
% Calculate the min without taking into account the missing values.
%
% INPUT
%  x    X-matrix/vector
%  dim  Direction, 1 - columnwise (default), 2 - rowwise
%  opt  opt=1: -Inf are removed.
%        Default: opt = 1
%
% OUTPUT
%  a    Min-values
%  i    Index
%
% See also: nanmax, nanmean, nanmedian, nanstd, nansum, nanvar

% 200819 AAR Swapped the order of inputs to be equal to 'nanmax'
% 180111 AAR Can now also calculate the minimum value in a specific
%             direction
% 121005 AAR Normally not interested in -Inf values
% 010905 AAR

if nargin < 2
    opt = 1;
    dim = 1;
end
if nargin < 3
    opt = 1;
end
if size( x, 1) == 1
    dim = 2;
end

if dim < 1 || dim > 2
    error( '''dim'' can only have the value ''1'' or ''2''')
end

x( isnan(x)) = Inf;
if opt == 1
    x( isinf(x)) = Inf;
end
[ a, i] = min( x, [], dim);
if opt == 1
    a( isinf(a)) = NaN;
end