function p=ncdf(x,m,s)
% p=ncdf(x,m,s)
%
% Cumulative normal distribution
%
% INPUT:
%  x  Value(s) to be investigated
%  m  Mean value. Default = 0
%  s  Standard deviation. Default = 1
%
% OUTPUT:
%  p  Probability that x<m
%
% See also: erfc, erf, mean, std

% 180905 AAR Can handle more than one column and one row of both 'm' and
%             's'
% 080805 AAR

if nargin<3
    s=1;
end

if nargin<2
    m=0;
end

%Redefine 'm' so that it has the right dimensions
if size(m,2)>1 & size(m,2)~=size(x,2)
    error('''m'' must have as many columns as ''x''')
elseif size(m,2)==1
    m=m*ones(1,size(x,2));
end
if size(m,1)>1 & size(m,1)~=size(x,1)
    error('''m'' must have as many rows as ''x''');
elseif size(m,1)==1
    m=ones(size(x,1),1)*m;
end

%Redefine 's' so that it has the right dimensions
if size(s,2)>1 & size(s,2)~=size(x,2)
    error('''s'' must have as many columns as ''x''')
elseif size(s,2)==1
    s=s*ones(1,size(x,2));
end
if size(s,1)>1 & size(s,1)~=size(x,1)
    error('''s'' must have as many rows as ''x''');
elseif size(s,1)==1
    s=ones(size(x,1),1)*s;
end

p=.5*erfc(-(x-m)./(s*sqrt(2)));