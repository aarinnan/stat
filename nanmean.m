function a=nanmean(x, dim)
% a=nanmean(x, dim)
%
% Calculate the mean without taking into account the missing values.
%
% INPUT
%  x    X-matrix/vector
%  dim  Direction. Default = 1 (column-wise), 2 = row-wise, 3 = depth, etc
%
% OUTPUT
%  a    Mean-values
%
% See also: NANMAX, NANMEDIAN, NANMIN, NANSTD, NANSUM, NANVAR

% 070720 AAR It should now work for all dimensions
% 090211 AAR Make the output similar to mean for dimensions
% 250111 AAR Made it work with dimensions
% 241105 AAR 
% 010905 AAR

x = x;
if nargin == 1
    if min( size( x) ) == 1
        x = vec( x);
    end
    dim = 1;
end

if dim < 1
    error( '''dim'' should be larger than 1')
end

c = ~isnan(x);
x( isnan(x)) = 0;

sumx = squeeze( sum( x, dim));
sumc = squeeze( sum( c, dim));
a = zeros( size( sumc) );
a( sumc > 0) = sumx( sumc > 0)./ sumc( sumc > 0);
a( sumc == 0) = NaN;
