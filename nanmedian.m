function a=nanmedian(x, dim)
% a=nanmedian(x, dim)
%
% Calculate the median without taking into account the missing values.
%
% INPUT
%  x    X-matrix/vector
%  dim  What dimensionality to use (1 is rows, 2 is columns, etc). <
%        default = 1 >
%
% OUTPUT
%  a  Median-values
%
% See also: nanmax, nanmean, nanmin, nanstd, nansum, nanvar

% 281212 AAR Added the direction of the analysis
% 130107 AAR Sometimes an empty matrix was put into the function
% 080905 AAR

[r,c]=size(x);
if nargin == 1
    dim = 1;
end

if r==0 || c==0
    a=x;
else
    if dim == 2
        if length( size( x) ) == 2
            x = x';
            c = r;
        else
            error( 'This feature needs coding')
        end
    end
    for i=1:c
        a(i)=median(x(~isnan(x(:,i)),i));
    end
end

if dim == 2
    if length( size( x) ) == 2
        a = a';
    end
end