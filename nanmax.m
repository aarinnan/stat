function [a,i]=nanmax(x, dim, opt)
% [a,i]=nanmax(x, dim, opt)
%
% Calculate the max without taking into account the missing values.
%
% INPUT
%  x    X-matrix/vector
%  dim  Dimension to search for the max-value (default = 1)
%  opt  If opt=0 Inf is the max-value. opt=1 indicates the max number not
%        taking Inf into account
%        Default: opt = 1
%
% OUTPUT
%  a  Max-values
%  i  The index
%
% See also: isnan, max

% 131211 AAR Included the direction as well
% 121005 AAR I am not normally interested in Inf as the maximum
% 010905 AAR

if nargin < 2
    dim = 1;
end
if size( x, 1) == 1
    dim = 2;
end

if nargin < 3
    opt=1;
end
x(isnan(x))=-Inf;
if opt==1
    x(isinf(x))=-Inf;
end
[a,i]=max(x, [], dim);
if opt==1
    a(isinf(a))=NaN;
end