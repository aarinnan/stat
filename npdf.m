function p=npdf(x,m,s)
% p=npdf(x,m,s)
%
% Normal probability distribution
%
% INPUT:
%  x  Value(s) to be investigated
%  m  Mean value. Default = 0
%  s  Standard deviation. Default = 1
%
% OUTPUT:
%  p  Probability that x<m
%
% See also: erfc, erf, mean, ncdf, std

% 100805 AAR

if min(size(x))>1
    error('''x'' can only have one column (or one row)')
else
    x=vec(x);
end

if nargin<3
    s=1;
end

if nargin<2
    m=0;
end

if length(m)>1
    if length(m)~=length(x)
        error('If ''m'' is more than one value it must have the same length as ''x''')
    end
    m=vec(m);
else
    m=ones(length(x),1)*m;
end

if length(s)>1
    if length(s)~=length(x)
        error('If ''s'' is more than one value it must have the same length as ''x''')
    end
    s=vec(s);
else
    s=ones(length(x),1)*s;
end

p=1./(sqrt(2*pi)*s).*exp(-.5*((x-m)./s).^2);

%------------------------------------
function xnew = vec( x)

xnew = x(:);